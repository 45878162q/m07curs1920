# Servei DNS

## Què vol dir DNS?

DNS significa Domaina Name System.

## Per a què serveix?

Per als humans és més fàcil recordar noms que números, per exemple és més fàcil recordar www.iespoblenou.org que 178.231.12.192

El servei DNS s'encarrega de traduïr de noms de domini a IPs i al contrari.

## Els inicis del DNS:

Al principi de la Arpanet, abans de que existís Internet com a tal, ja hi havia problemes amb la identificació dels equips o recursos, per aquest motiu existia un fitxer per a identificar-los.

Aquest fitxer era **hosts.txt** però quan va començar a crèixer la xarxa va esdevenir un sistema obsolet perquè no podia ser que totes les direccions depenguessin d'un fitxer que estaba dins un servidor que havia de distribuir-lo per tota Internet cada vegada que es feia una modificació.

A 1983 sorgeix el **Domain Name System**, una base de dades distribuïda i jeràrquica que emmagatzema noms de domini. 

És **distribuïda** perquè la informació no està tota a un repositori central, està distribuïda per diferents servidors DNSs d'Internet. 

És **jeràrquica** perquè s'organitza en una estructura de dominis que es poden compondre de subdominis que a la seva vegada es poden compondre de subdominis, per exemple: **virtual.ecaib.org**.

**org**.
org.**ecaib**.
org.ecaib.**virtual**

## Estructura del DNS:

El DNS fa servir la jeràrquia de dominis o arbre de dominis per ordenar els noms de domini, per exemple podem tenir dos dominis que siguin similars però que estiguin en una posició diferent del arbre de dominis:

google.com.
google.es.

En principi els dos noms son similars però amb l'estructura de dominis tenim que penjen de dues branques diferents de la jerarquia del arbre de dominis:

google.com.:

**.** (domini arrel)
.**com** (domini d'alt nivell .com Comercial)
.com.**google** (subdomini de Google)

google.es.:

**.** (domini arrel)
.**es** (domini d'alt nivell de país .es Espanya)
.es.**google**  (subdomini de Google)

Els dominis s'escriuen des de els nodes (subdominis o branques de l'arbre de jeràrquia) fins a l'arrel, però el DNS els identifica al revés, desde l'arrel fins a l'ultim node. 

Un **domini absolut** o **FQDN** és el que inclou tots els nodes des del domini fins l'arrel. Un **domini relatiu** no inclou l'arrel, normalment és relatiu al domini actual. 

Per exemple **virtual.ecaib.org.** és un domini absolut, mentre que **virtual** és un domini relatiu dins de ecaib.org i fa referència a virtual.ecaib.org.  

El DNS s'encarrega també de que no hi hagi cap domini repetit dins la base de dades, per evitar problemes de connexió.

## Zones:

Una zona és part de l'espai de noms de domini gestionada per un o més servidors DNS, per exemple podem tenir l'espai de noms següent:

**zona1**
.com 

**zona2**
youtube.com 
google.com
my.google.com
gmail.com

**zona3**
microsoft.com 
outlook.com

Aquí tenim *un* únic espai de noms amb *tres* zones i *sis* dominis, cada zona estarà gestionada per diferents servidors DNS que tindran la informació de domini dels equips de la zona i dels subdominis de la mateixa (assignats o no).

En l'exemple anterior el servidor que gestiona el domini .com delega la seva autoritat del domini als servidors de zona 2 i 3.

## Delegació:

Quan un servidor de zona delega l'autoritat de subdominis sobre una altra entitat, aquesta s'encarregarà de la gestió de noms de la seva zona.

En l'exemple anterior el servidor de zona1 delega autoritat dels subdominis youtube.com, google.com i gmail.com a Google, la qual s'encarregarà de gestionar aquests subdominis com vulgui.

El servidor de la zona1 guardarà la direcció dels subdominis de la zona2 i reenviarà la petició als servidors DNS que la gestionen.

L'estàndard DNS diu que hi haurà dos servidors autoritaris DNS a cada zona, el servidor primari i el secundari, per tal de proporcionar redundància, velocitat i seguretat.

## Resolució de noms:

Quan una aplicació necessita conèixer una IP partint d'un nom de domini donat (com per exemple una petició del navegador) es fa una petició per part del programa al servidor DNS assignat per la configuració del Sistema Operatiu.

Aquesta petició es conèix com **resolver**.

El servidor pot tenir emmagatzemada la direcció demanada i donar una resposta **autoritativa** o pot mantenir una còpia a la memòria cache i donar una resposta **no autoritativa**, però si directemant no sap la direcció demanada necessitarà fer la consulta a altres servidors de forma **recursiva** o **iterativa**.

Normalment quan es fa una petició *resolver* al servidor DNS es fa de manera **recursiva**, aixó pot provocar tres respostes posibles:

* El servidor DNS té la resposta emmagatzemada i la retorna de forma autoritativa.
* El servidor DNS té la resposta guardada a la caché, perquè ha fet una consulta anterior, i torna una resposta no autoritativa.
* El servidor DNS no té l'adreça demanada i fa una consulta iterativa a un altre servidor DNS conegut.


Les peticions **iteratives** entre servidors DNS tenen com a resposta, simplement, el millor nom de servidor DNS conegut pel servidor consultat, de forma que la resposta sigui més propera al servidor destí que es desitja.

